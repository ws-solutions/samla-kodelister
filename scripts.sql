-- CREATE (most of) THE TABLES

CREATE TABLE codes (
    code_id INTEGER PRIMARY KEY,
    name VARCHAR(255),
    classification_type VARCHAR(255),
    description VARCHAR(1024)
);


CREATE TABLE code_values (
    code_id INTEGER,
    code VARCHAR(255) NOT NULL,
    parent_code VARCHAR(255),
    level INTEGER,
    name VARCHAR(255),
    short_name VARCHAR(255),
    presentation_name VARCHAR(255),
    valid_from DATE NOT NULL,
    valid_to DATE NOT NULL DEFAULT (DATE('2499-12-31')),
    CONSTRAINT code_fk FOREIGN KEY (code_id) REFERENCES codes(code_id),
    CONSTRAINT code_pk PRIMARY KEY (code_id, code, valid_from)
);


CREATE TABLE correspondences (
    row_id INTEGER PRIMARY KEY AUTOINCREMENT,
    from_id INTEGER,
    from_code VARCHAR(255),
    from_name VARCHAR(255),
    from_short_name VARCHAR(255),
    to_id INTEGER,
    to_code VARCHAR(255),
    to_name VARCHAR(255),
    to_short_name VARCHAR(255),
    valid_from DATE NOT NULL,
    valid_to DATE NOT NULL DEFAULT (DATE('2499-12-31')),
    CONSTRAINT from_fk FOREIGN KEY (from_id, from_code) REFERENCES code_values(code_id, code),
    CONSTRAINT to_fk FOREIGN KEY (to_id, to_code) REFERENCES code_values(code_id, code)
);


CREATE TABLE changes (
    row_id INTEGER PRIMARY KEY AUTOINCREMENT,
    code_id INTEGER NOT NULL,
    from_code VARCHAR(255),
    from_name VARCHAR(255),
    from_short_name VARCHAR(255),
    to_code VARCHAR(255),
    to_name VARCHAR(255),
    to_short_name VARCHAR(255),
    valid_from DATE NOT NULL,
    valid_to DATE NOT NULL DEFAULT (DATE('2499-12-31')),
    CONSTRAINT code_fk FOREIGN KEY (code_id, from_code) REFERENCES code_values(code_id, code)
);

CREATE TABLE M_KOMMUNER (
    kommune_nr VARCHAR(4) PRIMARY KEY,
    kommune_navn VARCHAR(255),
    gjeldende_kommune_nr VARCHAR(4),
    gjeldende_kommune_navn VARCHAR(255),
    fylke_nr VARCHAR(4),
    CONSTRAINT fylke_fk FOREIGN KEY (fylke_nr) REFERENCES M_FYLKER(fylke_nr)
);

CREATE TABLE M_FYLKER (
    fylke_nr VARCHAR(4) PRIMARY KEY,
    fylke_navn VARCHAR(255),
    gjeldende_fylke_nr VARCHAR(4),
    gjeldende_fylke_navn VARCHAR(255)
);

CREATE TABLE M_KOSTRA_GRUPPER (
    kostra_grp_nr VARCHAR(12),
    kommune_nr VARCHAR(4),
    kostra_grp_navn VARCHAR(255),
    CONSTRAINT kommune_fk FOREIGN KEY (kommune_nr) REFERENCES M_KOMMUNER(kommune_nr)
);




CREATE VIEW V_KOMMUNE_PARENTS AS
    SELECT DISTINCT from_code AS kommune, to_code AS ny_kommune FROM changes
    WHERE code_id=131 AND from_code!=to_code
UNION ALL
    SELECT DISTINCT code AS kommune, NULL AS ny_kommune FROM code_values
        WHERE code NOT IN (
            SELECT from_code FROM changes
            WHERE from_code!=to_code
            )
        AND code_id=131
;

CREATE VIEW V_FYLKE_PARENTS AS
    SELECT DISTINCT from_code AS fylke, to_code AS nytt_fylke FROM changes
    WHERE code_id=127 AND from_code!=to_code
UNION ALL
    SELECT DISTINCT code AS fylke, NULL AS nytt_fylke FROM code_values
        WHERE code NOT IN (
            SELECT from_code FROM changes
            WHERE from_code!=to_code
            )
        AND code_id=127
    ;


CREATE VIEW V_FYLKE_GJELDENDE AS
WITH RECURSIVE cv AS (
    SELECT fylke, fylke AS orig_fylke, nytt_fylke, 0 AS level FROM V_FYLKE_PARENTS
    UNION ALL
    SELECT OG.fylke, IO.orig_fylke, OG.nytt_fylke, level + 1 AS level FROM V_FYLKE_PARENTS OG
     INNER JOIN cv IO ON IO.nytt_fylke  = OG.fylke
), cte_rn AS (
SELECT *
      ,ROW_NUMBER() OVER (PARTITION BY orig_fylke ORDER BY level DESC) rn
  FROM cv
)
SELECT orig_fylke, fylke AS gjeldende_fylke  FROM cte_rn
WHERE rn=1
;


CREATE VIEW V_KOMMUNE_GJELDENDE AS
WITH RECURSIVE cv AS (
    SELECT kommune, kommune AS orig_kommune, ny_kommune, 0 AS level FROM V_KOMMUNE_PARENTS
    UNION ALL
    SELECT OG.kommune, IO.orig_kommune, OG.ny_kommune, level + 1 AS level FROM V_KOMMUNE_PARENTS OG
     INNER JOIN cv IO ON IO.ny_kommune  = OG.kommune
), cte_rn AS (
SELECT *
      , ROW_NUMBER() OVER (PARTITION BY orig_kommune ORDER BY level DESC) rn
  FROM cv
)
SELECT orig_kommune, kommune AS gjeldende_kommune  FROM cte_rn
WHERE rn=1
;

CREATE VIEW V_M_KOMMUNER AS
    SELECT * FROM (
    SELECT code, name, ROW_NUMBER() over (PARTITION BY code ORDER BY valid_from DESC) AS rn_ FROM code_values
    WHERE code_id=131
    )
    WHERE rn_=1;

CREATE VIEW V_M_FYLKER AS
    SELECT * FROM (
    SELECT code, name, ROW_NUMBER() over (PARTITION BY code ORDER BY valid_from DESC) AS rn_ FROM code_values
    WHERE code_id=127
    )
    WHERE rn_=1;

INSERT INTO M_KOMMUNER
SELECT vm.code AS kommune_nr, vm.name AS kommune_navn, vkg.gjeldende_kommune AS gjeldende_kommune, rkj.name AS gjeldende_kommune_navn, SUBSTR(vm.code, 1, 2)||'00' AS fylke_nr FROM V_M_KOMMUNER vm
INNER JOIN V_KOMMUNE_GJELDENDE vkg ON vm.code=vkg.orig_kommune
INNER JOIN V_M_KOMMUNER rkj on vkg.gjeldende_kommune=rkj.code;

INSERT INTO M_FYLKER
SELECT vm.code AS fylke_nr, vm.name AS fylke_navn, vkg.gjeldende_fylke AS gjeldende_fylke, rkj.name AS gjeldende_kommune_navn FROM V_M_FYLKER vm
INNER JOIN V_FYLKE_GJELDENDE vkg ON vm.code=vkg.orig_FYLKE
INNER JOIN V_M_FYLKER rkj on vkg.gjeldende_fylke=rkj.code;

INSERT INTO M_KOSTRA_GRUPPER
SELECT from_code AS kostra_grp_nr, to_code AS kommune_nr, from_name AS kostra_grp_navn FROM correspondences
WHERE from_id=112;


