from sqlalchemy import create_engine, MetaData, Table
import requests
import json
from io import StringIO
import pandas as pd
import numpy as np
from datetime import datetime

db = create_engine('sqlite:///klass.db')
cnx = db.connect()

mtd = MetaData(cnx)
codes_tbl = Table('codes', mtd, autoload=True)
code_values_tbl = Table('code_values', mtd, autoload=True)
changes_tbl = Table('changes', mtd, autoload=True)
correspondence_tbl = Table('correspondences', mtd, autoload=True)


def date_parser(d):
    try:
        return datetime.strptime(d, '%Y-%m-%d')
    except:
        return datetime.strptime('2499-12-31', '%Y-%m-%d')


def populate_codes(codes):
    codesurl = f"http://data.ssb.no/api/klass/v1/classifications/{codes}"

    r = requests.get(codesurl, headers={'Accept': 'application/json', 'Encoding': 'utf-8'})
    codes_json = json.loads(r.text)

    insstmt = codes_tbl.insert().values(code_id=int(codes), name=codes_json['name'], classification_type=codes_json['classificationType'], description=codes_json['description'])
    cnx.execute(insstmt)


def populate_codelists(codes):
    codeslist_url = f"http://data.ssb.no/api/klass/v1/classifications/{codes}/codes?from=1900-01-01&to=2020-01-01"

    clr = requests.get(codeslist_url, headers={'Accept': 'application/json', 'Encoding': 'utf-8'})

    assert clr.ok

    codejson = json.loads(clr.text)['codes']

    for row in codejson:
        insstmt = code_values_tbl.insert().values(code_id=int(codes), code=row['code'],
                                            parent_code = row['parentCode'],
                                            level = int(row['level']),
                                            name = row['name'],
                                            short_name = row['shortName'],
                                            presentation_name = row['presentationName'],
                                            valid_from=date_parser(row['validFromInRequestedRange']),
                                            valid_to=date_parser(row['validToInRequestedRange'])
                                                  )
        cnx.execute(insstmt)


def populate_changes(codes, from_date):

    changes = requests.get(f"http://data.ssb.no/api/klass/v1/classifications/{codes}/changes?from={from_date}&to=2020-01-01", headers={'Accept': 'application/json'})

    assert changes.ok

    changejson = json.loads(changes.text)['codeChanges']

    for row in changejson:
        insstmt = changes_tbl.insert().values(
                                                code_id = int(codes),
                                                from_code=row['oldCode'],
                                                from_name=row['oldName'],
                                                from_short_name = row['oldShortName'],
                                                to_code = row['newCode'],
                                                to_name = row['newShortName'],
                                                to_short_name = row['newShortName'],
                                                valid_from=datetime.strptime(row['changeOccurred'], '%Y-%m-%d')
                                              )
        cnx.execute(insstmt)




def populate_correspondences(from_code, to_code):

    corresponds_endpoint = f"http://data.ssb.no/api/klass/v1/classifications/{from_code}/corresponds?targetClassificationId={to_code}&from=2014-01-01&to=2021-01-01"

    corr = requests.get(corresponds_endpoint, headers={'Accept': 'application/json'})

    assert corr.ok

    corrjson = json.loads(corr.text)['correspondenceItems']

    for row in corrjson:
        insstmt = correspondence_tbl.insert().values(
                                                from_id = int(from_code),
                                                from_code=row['sourceCode'],
                                                from_name=row['sourceName'],
                                                from_short_name = row['sourceShortName'],
                                                to_id = int(to_code),
                                                to_code = row['targetCode'],
                                                to_name = row['targetName'],
                                                to_short_name = row['targetShortName'],
                                                valid_from=date_parser(row['validFrom']),
                                                valid_to=date_parser(row['validTo'])
                                              )
        cnx.execute(insstmt)


#populate_correspondences('112', '131')
