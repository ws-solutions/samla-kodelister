# Kodelister

## Kjernedata

Dette repoet og disse datamodellene er basert på Klass, SSBs metadatatjeneste som inneholder bl.a. oversikt over kommuner, fylker og en del andre relevante kodelister. Kjernemodellen er dog ganske generiske (kanskje litt for generiske for mange), så en del andre kodelister skal kunne passe inn.

Kjernemodellen er sammensatt av 4 tabeller, som hver svarer til et endepunkt i klass:

- `codes`: Oversikt over kodelistene, én rad pr liste. I praksis mest nyttig for å finne relevant kodeliste-id.
- `code_values`: Selve kodelistene, hvor kolonnen `code` inneholder selve koden, og `name`, `short_name` etc inneholder beskrivelsen. Mindre intuitivt i denne tabellen er variablene `level` og `parent_code`, som gjør det mulig å ha hierarkiske kodelister, noe som er mye brukt i SSB.
- `changes`: Inneholder endringer fra én versjon av en kodeliste til en annen. I vårt tilfelle med kommuner og fylker vil tabellen inneholde kommune- og fylkesammenslåinger, gjennom feltene `from_code` som vil være gammelt kommunenummer (eller fylkesnummer), og `to_code` som vil være det nye nummeret.
- `correspondences` inneholder korrespondansetabeller fra én kodeliste til en annnen. Tabellen er p.t. lastet med korrespondanse mellom kommuner og KOSTRA kommunegrupperinger (som kan være aktuelt for enkelte brukere). Nøklene i tabellen er from_id/from_code som angir kodelisten og kodeverdien korrespondansen går fra, og to_id/to_code som er kodelisten og kodeverdien korrespondansen går til. Som regel er disse korrespondansene mange-til-en, altså ulike typer grupperinger, slik at retningen på korrespondansen er vesentlig.

Datamodell for kjernedataene:

![Kjernemodell](klass_core_datamodell.png)

## Tilrettelagte data ("mart")

Et antall tabeller spesifikt til Samla er under konstruksjon, basert på kjernedataene. Tentativt vil dette være oversikt over alle kommuner som har eksistert, men med siste kommunenavn (enkelte kommuner bytter navn uten å bytte kommunenummer, noe som er reflektert i `code_values`) tabellen. Denne tabellen skal i prinsippet inneholde alle kommuner som finnes i statistikkbanken, men unntak vil av ulike årsaker finnes. Det samme vil være tilfellet for fylker.

Av vesentlighet blir også en korrespondansetabell for hhv. kommuner og fylker, som gir nyeste kommune/fylke for alle kommune-/fylkesnummer. Siden enkelte kommuner er sammenslått flere ganger, er det ikke tilstrekkelig å bare koble på `changes` tabellen direkte. For øyeblikket er det kommunesammenslåinger som er relevant, men denne modellen blir for enkel ved kommunesplitting etc.

Dette området er fortsatt work-in-progress. Datamodell pr nå er:

![Mart Datamodell](klass_mart_datamodell.png)

## Script

Scriptet `main.py` inneholder kode som for en gitt kodeliste laster ned fra klass-apiet og inserter i SQLite-databasen. Det er egne funksjoner for hver av tabellene. Det vil alltid være aktuelt å populere `codes` og deretter `code_values` tabellene, men det er ikke alle tabeller som har endringer eller korrespondanselister knyttet til seg, så en 404-melding er ikke nødvendigvis et nederlag. For korrespondanselister må både fra-kodeliste og til-kodeliste spesifiseres.

på ssb.no/klass er det ikke nødvendigvis enkelt å finne frem til kodelister-id'er. Kodeliste-idene fremgår kun i URLen. For standard for fylkesinndeling er URLen https://www.ssb.no/klass/klassifikasjoner/127/, og api-endepunktet blir følgelig http://data.ssb.no/api/klass/v1/classifications/127. For korrespondansetabeller er forvirringen komplett, da korrespondansetabeller har et eget tabellnummer, som ikke må forveksles med hvilke kodelister korrespondansen går til/fra. I praksis må begge kodelistene slås opp separat for å finne de riktige kodelistenummerne.













gdf
